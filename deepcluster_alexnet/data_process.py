import pandas as pd
import numpy as np
from sktime.utils.load_data import load_from_tsfile_to_dataframe
from sklearn.preprocessing import MinMaxScaler

def preprocessing(data, n , l):
  x =np.array(data['val']).reshape(n,l).astype('float32')
  scaler = MinMaxScaler()
  data_normalized = scaler.fit_transform(x)
  return data_normalized

def read_dataset(path):
  raw_data, target = load_from_tsfile_to_dataframe(path)
  number_of_series = raw_data.shape[0]
  len_of_series = raw_data['dim_0'][0].shape[0]
  # print('Number of series = %d, length of series = %d' % (number_of_series, len_of_series))
  # print('Number of classes = %d' % len(np.unique(target)))

  data = pd.DataFrame(np.zeros((number_of_series * len_of_series, 3)),
                      columns=['id', 'time', 'val'])
  for series_id in range(number_of_series):
    # boarders for rows in data
    low = series_id * len_of_series
    high = (series_id + 1) * len_of_series
    # fill data with values: id, time, val
    data['id'][low:high] = series_id * np.ones(len_of_series)
    data['time'][low:high] = np.arange(0, len_of_series)
    data['val'][low:high] = raw_data['dim_0'][series_id].values

  data = preprocessing(data, number_of_series,len_of_series)

  return data, target