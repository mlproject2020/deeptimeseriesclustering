# HOW TO USE:
# PASTE
#
# dict_ts_lists = {label: indices for label, indices in enumerate(ts_lists)}
# sampler = UnifLabelSampler(len(np_y), dict_ts_lists)

# #### Dataset with new pseudolabels ####
# train_ds = data.TensorDataset(torch.from_numpy(np_x).float(), torch.from_numpy(np_y))
# trainloader = data.DataLoader(train_ds, batch_size=32, sampler=sampler)

import numpy as np
from torch.utils.data.sampler import Sampler

class UnifLabelSampler(Sampler):
    """Samples elements uniformely accross pseudolabels.
        Args:
            N (int): size of returned iterator.
            ts_lists: dict of key (target), value (list of data with this target)
    """

    def __init__(self, N, ts_lists):
        self.N = N
        self.ts_lists = ts_lists
        self.indexes = self.generate_indexes_epoch()

    def generate_indexes_epoch(self):
        nmb_non_empty_clusters = 0
        for i in range(len(self.ts_lists)):
            if len(self.ts_lists[i]) != 0:
                nmb_non_empty_clusters += 1

        size_per_pseudolabel = int(self.N / nmb_non_empty_clusters) + 1
        res = np.array([])

        for i in range(len(self.ts_lists)):
            # skip empty clusters
            if len(self.ts_lists[i]) == 0:
                continue
            indexes = np.random.choice(
                self.ts_lists[i],
                size_per_pseudolabel,
                replace=(len(self.ts_lists[i]) <= size_per_pseudolabel)
            )
            res = np.concatenate((res, indexes))

        np.random.shuffle(res)
        res = list(res.astype('int'))
        if len(res) >= self.N:
            return res[:self.N]
        res += res[: (self.N - len(res))]
        return res

    def __iter__(self):
        return iter(self.indexes)

    def __len__(self):
        return len(self.indexes)