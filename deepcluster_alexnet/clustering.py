import faiss
import numpy as np
import matplotlib.pyplot as plt

def visualize(X, centers, y_kmeans, d):
    print(centers.shape)
    print(centers[0].shape)
    plt.scatter(X[:, 0], X[:, 1], c=y_kmeans, s=50, cmap='viridis')
    plt.show()

def run_kmeans(x, nmb_clusters, visualize_clusters=True):
    """Runs kmeans on 1 GPU.
    Args:
        x: data
        nmb_clusters (int): number of clusters
    Returns:
        list: ids of data in each cluster
    """
    n_data, d = x.shape
    # faiss implementation of k-means
    clus = faiss.Clustering(d, nmb_clusters)

    # Change faiss seed at each k-means so that the randomly picked
    # initialization centroids do not correspond to the same feature ids
    # from an epoch to another.
    clus.seed = np.random.randint(1234)

    clus.niter = 10
    clus.max_points_per_centroid = 100
    res = faiss.StandardGpuResources()
    flat_config = faiss.GpuIndexFlatConfig()
    flat_config.useFloat16 = False
    flat_config.device = 0
    index = faiss.GpuIndexFlatL2(res, d, flat_config)

    # perform the training
    clus.train(x, index)
    _, I = index.search(x, 1) #ids of the k most similar vectors for each query vector
    losses = faiss.vector_to_array(clus.obj)
    centers = faiss.vector_to_array(clus.centroids).reshape(-1, d)
    if visualize_clusters:
        visualize(x,centers,I,d)
    losses
    return [int(n[0]) for n in I], losses[-1]


def preprocess_features(npdata, pca=15):
    """Preprocess an array of features.
    Args:
        npdata (np.array N * ndim): features to preprocess
        pca (int): dim of output
    Returns:
        np.array of dim N * pca: data PCA-reduced, whitened and L2-normalized
    """
    _, ndim = npdata.shape
    npdata =  npdata.astype('float32')

    # Apply PCA-whitening with Faiss
    # mat = faiss.PCAMatrix (ndim, pca, eigen_power=-0.5)
    # mat.train(npdata)
    # assert mat.is_trained
    # npdata = mat.apply_py(npdata)

    # L2 normalization
    row_sums = np.linalg.norm(npdata, axis=1)
    npdata = npdata / row_sums[:, np.newaxis]

    return npdata

def arrange_clustering(ts_lists):
    """Creates a dataset from clustering, with clusters as labels.
    Args:
        ts_lists (list of list): for each cluster, the list of time series indexes
                                    belonging to this cluster
    Returns:
        pseudolabels,corresponding to initial data points
    """
    pseudolabels = []
    ts_indexes = []
    for cluster, ts in enumerate(ts_lists):
        ts_indexes.extend(ts)
        pseudolabels.extend([cluster] * len(ts))
    indexes = np.argsort(ts_indexes)
    return np.asarray(pseudolabels)[indexes]



def cluster(data, k, visualize_clusters=False):
    """Performs k-means clustering.
        Args:
            x_data (np.array N * dim): data to cluster
    """
    xb = preprocess_features(data)

    # cluster the data
    I, loss = run_kmeans(xb, k, visualize_clusters=visualize_clusters)
    ts_lists = [[] for i in range(k)]
    for i in range(len(data)):
        ts_lists[I[i]].append(i)
    return loss, ts_lists