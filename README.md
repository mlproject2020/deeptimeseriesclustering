# DeepTimeSeriesClustering

Based on the paper 
### Deep Clustering for Unsupervised Learning of Visual Features 
(https://arxiv.org/pdf/1807.05520.pdf) by Caron et al
and its implementation (https://github.com/facebookresearch/deepcluster)


# How to use:

* Download [data](http://www.timeseriesclassification.com/dataset.php) or use our folder, where all data and implementation notebooks are stored [folder]( https://drive.google.com/open?id=1LsIewcjlBUXijBoPr0h7tS88-jeSPATE)

* Open and run `our_solution.ipynb`

* Baseline, a model without clustering, is stored in the `baseline.ipynb` file.