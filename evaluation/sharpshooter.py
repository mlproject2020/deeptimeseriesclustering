import pandas as pd
import matplotlib.pyplot as plt

def sharpshooter_plot(acc_train, acc_test):
    """
    Comparison of current model with 1-nn Euclidian Distance
    
    acc_train : dict, key is a dataset name, value is an accuracy on train 
    acc_test : dict, key is a dataset name, value is an accuracy on test 
    """
    
    # load 1-nn results
    nn_res = pd.read_csv('1nn_results.csv')
    nn_acc_train = {data: acc / 100 for data, acc in zip(nn_res['dataset'], nn_res['train_accuracy'])}
    nn_acc_test = {data: acc / 100 for data, acc in zip(nn_res['dataset'], nn_res['test_accuracy'])}
    
    # TODO CHECK MATCHING of dataset names?
    
    # Expected Accuracy Gain
    exp_gain = [acc_train[data] / nn_acc_train[data] for data in nn_res['dataset']]
    
    # Actual Accuracy Gain
    act_gain = [acc_test[data] / nn_acc_test[data] for data in nn_res['dataset']]
    
    fig = plt.figure(figsize=(8, 8))
    plt.rcParams.update({'font.size': 14})
    plt.fill_between([1, max(exp_gain)], [1, 1], [max(act_gain), max(act_gain)], alpha=0.3)
    plt.fill_between([min(exp_gain), 1], [1, 1], [min(act_gain), min(act_gain)], alpha=0.3)    
    plt.plot(exp_gain, [1] * len(exp_gain)) # line y = 1
    plt.plot([1] * len(act_gain), act_gain) # line x = 1
    plt.scatter(exp_gain, act_gain, c='black')
    plt.text((max(exp_gain) + 1) / 2, (max(act_gain) + 1) / 2, 'TP', fontsize=18)
    plt.text((max(exp_gain) + 1) / 2, (min(act_gain) + 1) / 2, 'FP', fontsize=18)
    plt.text((min(exp_gain) + 1) / 2, (max(act_gain) + 1) / 2, 'FN', fontsize=18)
    plt.text((min(exp_gain) + 1) / 2, (min(act_gain) + 1) / 2, 'TN', fontsize=18)
    plt.title('Texas Sharpshooter Plot')
    plt.xlabel('Expected Accuracy Gain')
    plt.ylabel('Actual Accuracy Gain')
    
    fig.savefig('sharpshooter_plot.png')
    
    
# draw test

from random import randrange

nn_res = pd.read_csv('1nn_results.csv')
nn_res.head(2)

# random generation of accuracies
acc_train = {data: randrange(80, 100) / 100 for data in nn_res['dataset']}
acc_test = {data: randrange(80, 100) / 100 for data in nn_res['dataset']}

sharpshooter_plot(acc_train, acc_test)