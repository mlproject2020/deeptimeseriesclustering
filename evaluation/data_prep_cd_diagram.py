import pandas as pd

ucr_results = pd.read_csv('singleTrainTest.csv')

# list of models
models = list(ucr_results.columns[1:])


# rename columns
ucr_results.columns = ['dataset_name'] + models

# number of datasets
n_datasets = ucr_results.shape[0] # 85 for UCR

def single_df(model, n_datasets, ucr_results):
    df = pd.DataFrame(columns=['classifier_name', 'dataset_name', 'accuracy'])
    df['dataset_name'] = ucr_results['dataset_name']
    df['accuracy'] = ucr_results[model]
    df['classifier_name'] = model
    return df

frames = [single_df(model, n_datasets, ucr_results) for model in models]

# TODO add OUR results 

res = pd.concat(frames)
res.to_csv('results_for_cd_diagram.csv', index=None)
