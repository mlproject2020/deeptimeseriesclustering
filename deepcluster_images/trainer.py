import torch
import torch.optim as optim
from torch.nn import CrossEntropyLoss
from torch.utils import data
import numpy as np

from ts_models import TS_CNN

################################## Loading Data #########################################
np_x = np.random.normal(0, 1, (500, 100))
np_x = np.expand_dims(np_x, axis=1)
np_y = np.random.randint(0, 4, (500,), dtype=np.int64)  #just random labels for the very first iteration of feature extraction

train_ds = data.TensorDataset(torch.from_numpy(np_x).float(), torch.from_numpy(np_y))
trainloader = data.DataLoader(train_ds, batch_size=32, shuffle=True)

################################## Creating the model ###################################
net = TS_CNN(channels=[1, 128, 128, 128],
             pools=[2, 2, 2],
             feature_size=256,
             num_classes=4)
criterion = CrossEntropyLoss()
optimizer = optim.SGD(net.parameters(), lr=0.001, momentum=0.9)

################################## Traing loop ##########################################
for epoch in range(2):
    running_loss = 0.0

    #### Extracting features for clustering ####
    all_features = []
    net.eval()
    with torch.no_grad():
        for i, batch in enumerate(trainloader, 0):
            inputs, _ = batch
            feats, _ = net(inputs)
            all_features.append(feats.numpy())
    net.train()

    all_features = np.vstack(all_features)

    #### !!FAISS CLUSTERING HERE!! ####
    np_y = np.random.randint(0, 4, (500,), dtype=np.int64)  #change the random label generation to clustering

    #### Dataset with new pseudolabels ####
    train_ds = data.TensorDataset(torch.from_numpy(np_x).float(), torch.from_numpy(np_y))
    trainloader = data.DataLoader(train_ds, batch_size=32, shuffle=True)

    #### Regular Forward/Backward passes ####
    for i, batch in enumerate(trainloader, 0):
        inputs, labels = batch
        optimizer.zero_grad()

        feats, outputs = net(inputs)
        loss = criterion(outputs, labels)
        loss.backward()
        optimizer.step()

        running_loss += loss.item()
        if i % 2000 == 1999:
            print('[%d, %5d] loss: %.3f' %
                  (epoch + 1, i + 1, running_loss / 2000))
            running_loss = 0.0

print('Finished Training')