import torch
import torch.nn as nn
import torch.nn.functional as F


class TS_CNN(nn.Module):
    def __init__(self, channels, pools, feature_size, num_classes):
        super(TS_CNN, self).__init__()
        self.cnn = nn.Sequential(
            nn.Conv1d(channels[0], channels[1], 5),
            nn.MaxPool1d(pools[0]),
            nn.BatchNorm1d(channels[1]),
            nn.ReLU(),
            nn.Conv1d(channels[1], channels[2], 5),
            nn.MaxPool1d(pools[1]),
            nn.BatchNorm1d(channels[2]),
            nn.ReLU(),
            nn.Conv1d(channels[2], channels[3], 5),
            nn.MaxPool1d(pools[2]),
            nn.BatchNorm1d(channels[3]),
            nn.ReLU(),
            nn.AdaptiveMaxPool1d(1)
        )
        self.cnn.apply(weights_init)
        self.features = nn.Sequential(
            nn.Linear(channels[3], feature_size),
            nn.ReLU())
        self.features.apply(weights_init)
        self.classifier = nn.Linear(feature_size, num_classes)
        self.classifier.apply(weights_init)

    def forward(self, x):
        cnn_out = self.cnn(x)
        cnn_out = torch.flatten(cnn_out, start_dim=1)
        feats = self.features(cnn_out)
        preds = self.classifier(feats)
        return feats, preds, F.softmax(preds)


def weights_init(m):
    classname = m.__class__.__name__
    if classname.find('Conv') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
    elif classname.find('Linear') != -1:
        nn.init.normal_(m.weight.data, 0.0, 0.02)
        nn.init.constant_(m.bias.data, 0.0)
    elif classname.find('BatchNorm') != -1:
        nn.init.normal_(m.weight.data, 1.0, 0.02)
        nn.init.constant_(m.bias.data, 0.0)


